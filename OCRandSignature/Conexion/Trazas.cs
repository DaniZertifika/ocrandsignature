﻿using System;
using System.IO;
using System.Diagnostics;

namespace conexion
{
    public class Trazas
    {
        public Trazas(string metodo, string clase, int usuario, string descripcion, string error)
        {

            try
            {
                //string trazasActivas = WebConfigurationManager.AppSettings["trazasActivas"];
                //if (trazasActivas.Equals("True"))
                //{
                StackTrace stackTrace = new StackTrace();
                hilo h = new hilo(metodo, clase, usuario, descripcion, error + " -> " + stackTrace.ToString());
                h.guardarTraza();
                //}


            }
            catch (Exception e)
            {
            }
        }

        private class hilo
        {
            string metodo = "";
            string clase = "";
            int usuario = -1;
            string descripcion = "";
            string error = "";

            public hilo(string metodo, string clase, int usuario, string descripcion, string error)
            {
                this.metodo = metodo;
                this.clase = clase;
                this.usuario = usuario;
                this.descripcion = descripcion;
                this.error = error;
                if (this.metodo.Length > 50) metodo.Substring(0, 50);
                if (this.clase.Length > 50) clase.Substring(0, 50);


            }

            public void guardarTraza()
            {
                try
                {
                    System.IO.Directory.CreateDirectory("c:/Temp/" + "trazas/");
                    File.WriteAllText("c:/Temp/" + "trazas/" + metodo + "-" + Guid.NewGuid() + ".txt", metodo + "-" + clase + "-" + usuario + "-" + descripcion + "-" + error);
                }
                catch (Exception e)
                {
                }
            }
        }



    }
}