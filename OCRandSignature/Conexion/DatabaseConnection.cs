﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace conexion
{
    public class DatabaseConnection
    {
        public static DataTable executeNonQueryDT(String s, CommandType c, String strConex)
        {
            SqlConnection sql1 = new SqlConnection(strConex);
            DataTable dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader reader;

                    cmd.CommandText = s;
                    cmd.CommandType = c;
                    cmd.Connection = sql1;
                    //cmd.Parameters.Add(param);

                    sql1.Open();
                    reader = cmd.ExecuteReader();
                    dt = new DataTable();
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("executeNonQueryDT", "DatabaseConnection", -1, "q:" + s + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return dt;
            }
            finally
            {
                sql1.Close();
            }
        }

        public static DataTable executeNonQueryDT(String s, CommandType c, String strConex, List<SqlParameter> lista)
        {
            SqlConnection sql1 = new SqlConnection(strConex);
            DataTable dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader reader;

                    cmd.CommandText = s;
                    cmd.CommandType = c;
                    cmd.Connection = sql1;
                    foreach (SqlParameter sp in lista)
                    {
                        cmd.Parameters.Add(sp);
                    }
                    sql1.Open();
                    reader = cmd.ExecuteReader();
                    dt = new DataTable();
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("executeNonQueryDT", "DatabaseConnection", -1, "q:" + s + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return dt;
            }
            finally
            {
                sql1.Close();
            }
        }

        public static int executeNonQueryInt(String s, CommandType c, String strConex)
        {
            SqlConnection sql1 = new SqlConnection(strConex);
            int res = -1;
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandText = s;
                    cmd.CommandType = c;
                    cmd.Connection = sql1;

                    sql1.Open();
                    res = cmd.ExecuteNonQuery();
                }
                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("executeNonQueryInt", "DatabaseConnection", -1, "q:" + s + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return res;
            }
            finally
            {
                sql1.Close();
            }
        }

        public static object executeScalar(String s, CommandType c, String strConex, List<SqlParameter> lista)
        {
            SqlConnection sql1 = new SqlConnection(strConex);
            object res = null;
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandText = s;
                    cmd.CommandType = c;
                    cmd.Connection = sql1;
                    foreach (SqlParameter sp in lista)
                    {
                        cmd.Parameters.Add(sp);
                    }

                    sql1.Open();
                    res = cmd.ExecuteScalar();
                }
                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("executeNonQueryScalar", "DatabaseConnection", -1, "q:" + s + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return res;
            }
            finally
            {
                sql1.Close();
            }
        }

        public static object executeScalar(String s, CommandType c, String strConex)
        {
            SqlConnection sql1 = new SqlConnection(strConex);
            object res = null;
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandText = s;
                    cmd.CommandType = c;
                    cmd.Connection = sql1;

                    sql1.Open();
                    res = cmd.ExecuteScalar();
                }
                return res;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("executeNonQueryScalar", "DatabaseConnection", -1, "q:" + s + "-" + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return res;
            }
            finally
            {
                sql1.Close();
            }
        }

        public static string executeScalarString(String s, CommandType c, String strConex)
        {
            SqlConnection sql1 = new SqlConnection(strConex);
            object res = null;
            try
            {
                if (!string.IsNullOrEmpty(s))
                {
                    SqlCommand cmd = new SqlCommand();

                    cmd.CommandText = s;
                    cmd.CommandType = c;
                    cmd.Connection = sql1;

                    sql1.Open();
                    res = cmd.ExecuteScalar();
                }
                if (res != null && !Convert.IsDBNull(res) && !string.IsNullOrEmpty(res.ToString())) return res.ToString();
                else return string.Empty;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("executeNonQueryScalarString", "DatabaseConnection", -1, "q:" + s + "-" + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return string.Empty;
            }
            finally
            {
                sql1.Close();
            }
        }
    }
}