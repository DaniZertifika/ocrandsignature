﻿using System;
using System.IO;
using System.Windows;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Threading;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Net;
using conexion;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using System.Collections.Generic;

namespace OCRandSignature
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        string path;
        string ID;
        List<X509Certificate2> certificados = new List<X509Certificate2>();

        private static Action EmptyDelegate = delegate () { };


        public MainWindow()
        {
            InitializeComponent();
        }



        public int FileUpload(string SourceFilepath)
        {
            try
            {
                WebClient client = new WebClient();

                //Credential information for remote Mechine.-> here I used Username=erandika1986 and Password=123
                NetworkCredential nc = new NetworkCredential("administrador", "Zertifik4");

                //Here we have to give destination information
                Uri addy = new Uri(@"\\192.168.1.197\c$\FIRMA Y OCR/" + SourceFilepath.Split('\\')[SourceFilepath.Split('\\').Length - 1]);

                //Here we set the the credential information to the Webclient
                client.Credentials = nc;

                //upload the file from Source to the destination
                client.UploadFile(addy, SourceFilepath);

                return 1;

            }
            catch (Exception ex1)
            {
                return 0;
            }

        }


        private void fileExplorer_Click(object sender, RoutedEventArgs e)
        {

            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:/Temp";
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                path = dialog.FileName;
                //System.Windows.Forms.MessageBox.Show(dialog.FileName);
                textBoxRuta.Text = dialog.FileName;
            }

            string origen = path;

            try
            {

                foreach (var directorio in Directory.GetDirectories(origen))
                {
                    vaciarDirectorio(directorio.ToString(), "\\192.168.1.197/c$/FIRMA Y OCR/");
                }
                foreach (var file in Directory.GetFiles(origen))
                {
                    File.Copy(file.ToString(), @"\\192.168.1.197/c$/FIRMA Y OCR/" + file.Split('\\')[file.Split('\\').Length - 1]);
                }

            }
            catch (Exception ex)
            {

            }


            buttonOCR.IsEnabled = true;

        }

        private void vaciarDirectorio(string directorios, string destino)
        {

            try
            {

                foreach (var directorio in Directory.GetDirectories(directorios))
                {
                    vaciarDirectorio(directorio, destino);
                }
                foreach (var file in Directory.GetFiles(directorios))
                {
                    string origen = file.ToString(); //IDs[numeroFicheros];
                    File.Copy(origen, @"\\192.168.1.197/c$/FIRMA Y OCR/" + file.Split('\\')[file.Split('\\').Length - 1]);
                }

            }
            catch (Exception ex)
            {

            }



        }

        private void buttonOCR_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                //Preparacion de la interficie
                LoadingOCR.Visibility = Visibility.Visible;
                LoadingOCR.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);


                //Cuerpo del programa
                //obtencion de la ID de usuario y empresa
                string query = "SELECT Usuarios.idUsuario, Empresa.idEmpresa from Usuarios inner join UsuarioEmpresa inner join Empresa on empresa.idEmpresa = usuarioempresa.idempresa on usuarioempresa.idusuario = usuarios.idusuario where usuarios.nombre = '" + comboBoxNombreUsuario.SelectedItem.ToString() + "' and empresa.nombre = '" + comboBoxNombreEmpresa.SelectedItem.ToString() + "'";
                DataTable result = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionAnywhereLocal);

                //wszfk.Service1 conexionWSZFK = new wszfk.Service1();
                //string ID = conexionWSZFK.ReservarIdDocumento(Directory.GetFiles(@"\\192.168.1.197\c$\FIRMA Y OCR/A/").Length, result.Rows[0].ItemArray[0].ToString(), result.Rows[0].ItemArray[1].ToString());

                ID = "3164|3165|3166|3167|3168|";
                string[] IDs = ID.Split('|');

                int numeroFicheros = 0;
                foreach (var file in Directory.GetFiles(@"\\192.168.1.197\c$\FIRMA Y OCR/"))
                {
                    string origen = @"\\192.168.1.197/FIRMA Y OCR/" + file.Split('/')[file.Split('/').Length - 1]; //IDs[numeroFicheros];
                    string destino = @"\\192.168.1.197/c$/Temp/OCR/" + IDs[numeroFicheros] + "." + file.Split('.')[file.Split('.').Length - 1];
                    File.Move(origen, destino);
                    numeroFicheros++;
                }

                while ((Directory.GetFiles(@"\\192.168.1.197\c$\Temp\OCRout/").Length + Directory.GetFiles(@"\\192.168.1.197\c$\Temp\OCRExcepciones/").Length / 2) < numeroFicheros)
                {
                    System.Threading.Thread.Sleep(5000);
                }

                //insercion en la base de datos
                int resultado;
                int j = 0;
                SqlConnection vCon = new SqlConnection(ConnectionString.CadenaConexionAnywhereLocal);
                vCon.Open();


                foreach (var file in Directory.GetFiles(@"\\192.168.1.197\c$\Temp\OCRout/"))
                {
                    try
                    {
                        if (ID.Contains(file.Split('/')[file.Split('/').Length - 1].Replace(".txt", "")))
                        {

                            query = "INSERT INTO OCR (idDocumento, idUsuario, estado) values (" + IDs[j] + "," + result.Rows[0].ItemArray[0].ToString() + "," + 1 + ")";
                            resultado = DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.CadenaConexionAnywhereLocal);

                            string texto = System.IO.File.ReadAllText(@"\\192.168.1.197\c$/Temp/OCRout/" + IDs[j] + ".txt");
                            SqlCommand comando = new SqlCommand("insert into documentosocr (iddocumento,textoocr) values (@iddocumento,@texto)");
                            comando.Connection = vCon;
                            comando.Parameters.AddWithValue("@iddocumento", IDs[j].Replace(".txt", ""));
                            comando.Parameters.AddWithValue("@texto", texto);
                            comando.ExecuteNonQuery();
                            j++;
                            File.Move(file, "\\\\192.168.1.197/c$/Temp/OCRprocesado/TXT/" + file.Split('/')[file.Split('/').Length - 1]);

                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

                // Actualización de la ventana
                LoadingOCR.Visibility = Visibility.Hidden;
                vistoOCR.Visibility = Visibility.Visible;
                buttonOCR.IsEnabled = false;
                buttonFirmar.IsEnabled = true;

            }
            catch (Exception ex)
            {

            }



        }


        private void buttonFirmar_Click(object sender, RoutedEventArgs e)
        {

            LoadingFirmar.Visibility = Visibility.Visible;
            LoadingFirmar.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);


            //Code here
            //Firma de documentos


            foreach (var file in Directory.GetFiles(@"\\192.168.1.197/c$/Temp/OCRoutPDF/"))
            {

                //si es un documento que se tiene que subir al servidor
                if (ID.Contains(file.Split('/')[file.Split('/').Length - 1].Replace(".pdf", "")))
                {
                    byte[] bytes = File.ReadAllBytes(file);
                    X509Certificate2 Certificate = certificados[comboBoxCertificado.SelectedIndex];
                    bytes = sign(bytes, Certificate);
                    File.WriteAllBytes(file, bytes);

                }

            }



            //Al acabar subir contenido de TXT a la base de datos


            LoadingFirmar.Visibility = Visibility.Hidden;
            vistoFirmar.Visibility = Visibility.Visible;

            buttonFirmar.IsEnabled = false;
            buttonSubir.IsEnabled = true;
        }

        private void buttonSubir_Click(object sender, RoutedEventArgs e)
        {

            //subida real
            int i = 0;
            foreach (var file in Directory.GetFiles(@"\\192.168.1.197/c$/Temp/OCRoutPDF/"))
            {

                //si es un documento que se tiene que subir al servidor
                if (ID.Contains(file.Split('/')[file.Split('/').Length - 1].Replace(".pdf", "")))
                {
                    string ruta = getRutaDoc(ID.Split('|')[i]);
                    ruta = ruta.Replace(".pdf", "");
                    ruta = ruta.Remove(ruta.Length - 2);
                    ruta = ruta.Replace("/", "\\");

                    Directory.CreateDirectory("\\" + ruta);
                    File.Move(file.Replace("/", "\\"), "\\" + getRutaDoc(ID.Split('|')[i]));
                    i++;
                    //File.Delete(file);

                }

            }


            vistoFirmar.Visibility = Visibility.Hidden;
            vistoOCR.Visibility = Visibility.Hidden;


            buttonOCR.IsEnabled = false;
            buttonFirmar.IsEnabled = false;
            buttonSubir.IsEnabled = false;
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //rellenar la combo box de empresas
            string query = "SELECT Nombre from Empresa where nombre is not null and nombre != '' ";
            DataTable result = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionAnywhereLocal);
            for (int i = 0; i < result.Rows.Count; i++)
            {
                comboBoxNombreEmpresa.Items.Add(result.Rows[i].ItemArray[0]);
            }
            comboBoxNombreEmpresa.SelectedIndex = 0;

            //rellenado con los certificados
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            try
            {
                foreach (X509Certificate2 certificate in store.Certificates)
                {
                    if (!certificate.FriendlyName.Equals(""))
                    {
                        string emitidoPara = certificate.GetName();
                        emitidoPara = emitidoPara.Substring(emitidoPara.IndexOf("CN=") + "CN=".Length).Split(',')[0];
                        comboBoxCertificado.Items.Add(emitidoPara); //certificate.NotAfter
                        certificados.Add(certificate);
                    }

                }
            }
            catch (Exception)
            {
                //si falla lo limpiamos todo y rellenamos con el nombre descriptivo
                comboBoxCertificado.Items.Clear();
                foreach (X509Certificate2 certificate in store.Certificates)
                {
                    if (!certificate.FriendlyName.Equals(""))
                    {
                        comboBoxCertificado.Items.Add(certificate.FriendlyName); //certificate.NotAfter
                    }

                }
            }

            comboBoxCertificado.SelectedIndex = 0;
            store.Close();

        }



        private void comboBoxNombreEmpresa_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            comboBoxNombreUsuario.Items.Clear();
            string query = "SELECT Usuarios.Nombre from Usuarios inner join UsuarioEmpresa inner join Empresa on empresa.idEmpresa = usuarioempresa.idempresa on usuarioempresa.idusuario = usuarios.idusuario where usuarios.nombre != '' and empresa.nombre = '" + comboBoxNombreEmpresa.SelectedItem.ToString() + "'";
            DataTable result = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.CadenaConexionAnywhereLocal);
            for (int i = 0; i < result.Rows.Count; i++)
            {
                comboBoxNombreUsuario.Items.Add(result.Rows[i].ItemArray[0]);
            }
            comboBoxNombreUsuario.SelectedIndex = 0;
        }

        public static string getRutaDoc(string idDoc)
        {

            string path = "00000000".Substring(idDoc.Length) + idDoc;
            if (Convert.ToInt32(idDoc) < 10) idDoc = "0" + idDoc;

            string pathString = "\\192.168.1.197/c$/inetpub/wwwroot/signesDemosVolumenes/" + path.Substring(0, 2) + "/" + path.Substring(2, 2) + "/" +
                    path.Substring(4, 2) + "/" + idDoc.Substring(idDoc.Length - 2, 2) + ".pdf";

            return pathString;
        }


        public static byte[] sign(byte[] Source, X509Certificate2 Certificate)
        {

            try
            {

                PdfReader objReader = new PdfReader(Source);
                MemoryStream ms = new MemoryStream();
                PdfStamper objStamper = PdfStamper.CreateSignature(objReader, ms, '\0', null, true);
                PdfSignatureAppearance objSA = objStamper.SignatureAppearance;

                Org.BouncyCastle.X509.X509CertificateParser cp = new Org.BouncyCastle.X509.X509CertificateParser();
                Org.BouncyCastle.X509.X509Certificate[] chain = new Org.BouncyCastle.X509.X509Certificate[] { cp.ReadCertificate(Certificate.RawData)};

                IExternalSignature externalSignature = new X509Certificate2Signature(Certificate, "SHA-1");
                PdfSignatureAppearance signatureAppearance = objStamper.SignatureAppearance;

                ITSAClient tsa = new TSAClientBouncyCastle("http://www.signes30.com:85/ts.inx", "UsuarioInicial", "CaIni_14", 8192, "SHA-1");

                MakeSignature.SignDetached(signatureAppearance, externalSignature, chain, null, null, tsa, 0, CryptoStandard.CMS);

                return ms.ToArray();
            }
            catch (Exception e)
            {
                return null;
            }
        }



    }
}
